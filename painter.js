

class Painter {
    constructor(canvas) {
        this.innitialize(canvas);
    }

    innitialize(canvas){
        // console.log(this);
        this.canvas = canvas;
        this.width = this.canvas.width = window.innerWidth * 0.8;
        this.height = this.canvas.height = window.innerHeight * 0.8;
        this.mode = 0; //0: pencil, 1: eraser, 2: circle, 3: rectangle, 4: triangle, 5: text. 6: line
        this.rectEmpty = false;
        this.triEmpty = false;
        this.circleEmpty = false;
        this.painting = false;
        this.ctx = this.canvas.getContext("2d");
        this.lineWidth = this.ctx.lineWidth = 6;
        this.ctx.lineCap = "round";
        this.ctx.lineJoin = "round";
        this.StartX = 0;
        this.StartY = 0;
        this.color = this.ctx.fillStyle = "blue";
        this.color = this.ctx.strokeStyle = "blue";
        this.rainbowColor = 0;
        this.StartImg = this.ctx.getImageData(0, 0, this.width, this.height);;
        this.undoStack = [];
        this.redoStack = [];
        this.fontSize = 12;
        this.saveHistory();

        document.getElementsByClassName("option-btn")[0].addEventListener('click', ()=>{
            this.canvas.style.cursor = "url(./img/pencil.png) 0 32, auto";
            this.mode = 0;
        });

        document.getElementsByClassName("option-btn")[1].addEventListener('click', ()=>{
            this.canvas.style.cursor = "url(./img/text.png) 0 32, auto";
            this.mode = 5;
        });


        document.getElementsByClassName("option-btn")[2].addEventListener('click', ()=>{
            this.canvas.style.cursor = "url(./img/eraser.png) 0 32, auto";
            this.mode = 1;
        });

        document.getElementsByClassName("option-btn")[3].addEventListener('click', ()=>{
            if(this.circleEmpty){
                this.canvas.style.cursor = "url(./img/circleEmpty.png) 0 32, auto";
            }
            else{
                this.canvas.style.cursor = "url(./img/circle.png) 0 32, auto";
            }
            this.mode = 2;
        });

        document.getElementsByClassName("option-btn")[3].addEventListener('dblclick', ()=>{
            if(this.circleEmpty){
                this.canvas.style.cursor = "url(./img/circle.png) 0 32, auto";
                document.getElementsByClassName("option-btn")[3].src="./img/circle.svg";
            }
            else{
                this.canvas.style.cursor = "url(./img/circleEmpty.png) 0 32, auto";
                document.getElementsByClassName("option-btn")[3].src="./img/circleEmpty.svg"
            }
            this.mode = 2;
            this.circleEmpty = !this.circleEmpty;
        });

        document.getElementsByClassName("option-btn")[4].addEventListener('click', ()=>{
            if(this.rectEmpty){
                this.canvas.style.cursor = "url(./img/rectangleEmpty.png) 0 32, auto";
            }
            else{
                this.canvas.style.cursor = "url(./img/rectangle.png) 0 32, auto";
            }
            this.mode = 3;
        });

        document.getElementsByClassName("option-btn")[4].addEventListener('dblclick', ()=>{
            if(this.rectEmpty){
                this.canvas.style.cursor = "url(./img/rectangle.png) 0 32, auto";
                document.getElementsByClassName("option-btn")[4].src="./img/rectangle.svg";
            }
            else{
                this.canvas.style.cursor = "url(./img/rectangleEmpty.png) 0 32, auto";
                document.getElementsByClassName("option-btn")[4].src="./img/rectangleEmpty.svg"
            }
            this.mode = 3;
            this.rectEmpty = !this.rectEmpty;
        });

        document.getElementsByClassName("option-btn")[5].addEventListener('click', ()=>{
            if(this.triEmpty){
                this.canvas.style.cursor = "url(./img/triangleEmpty.png) 0 32, auto";
            }
            else{
                this.canvas.style.cursor = "url(./img/triangle.png) 0 32, auto";
            }
            this.mode = 4;
        });

        document.getElementsByClassName("option-btn")[5].addEventListener('dblclick', ()=>{
            if(this.triEmpty){
                this.canvas.style.cursor = "url(./img/triangle.png) 0 32, auto";
                document.getElementsByClassName("option-btn")[5].src="./img/triangle.svg";
            }
            else{
                this.canvas.style.cursor = "url(./img/triangleEmpty.png) 0 32, auto";
                document.getElementsByClassName("option-btn")[5].src="./img/triangleEmpty.svg"
            }
            this.mode = 4;
            this.triEmpty = !this.triEmpty;
        });
        
        document.getElementsByClassName("option-btn")[6].addEventListener('click', ()=>{
            this.canvas.style.cursor = "url(./img/line.png) 0 32, auto";
            this.mode = 6;
        });

        document.getElementsByClassName("option-btn")[7].addEventListener('click', ()=>{
            this.downloadImage();
        });

        document.getElementsByClassName("option-btn")[8].addEventListener('change', (e)=>{
            const reader = new FileReader();
            const img = new Image();
            this.saveHistory;
            reader.onload = () => {
                img.onload = () => {
                    this.ctx.drawImage(img, 0, 0, this.width, this.height);
                };
                img.src = reader.result;
            };
            reader.readAsDataURL(e.target.files[0]);
        });

        document.getElementsByClassName("option-btn")[9].addEventListener('click', ()=>{
            if(this.undoStack.length == 0){
                console.log("empty!");
            }
            else{
                const redoData = this.ctx.getImageData(0, 0, this.width, this.height);
                this.redoStack.push(redoData);
                const undoData = this.undoStack.pop();
                this.ctx.putImageData(undoData, 0, 0);
            }            
        });

        document.getElementsByClassName("option-btn")[10].addEventListener('click', ()=>{
            if(this.redoStack.length == 0){
                console.log("empty!");
            }
            else{
                const undoData = this.ctx.getImageData(0, 0, this.width, this.height);
                this.undoStack.push(undoData);
                const redoData = this.redoStack.pop();
                this.ctx.putImageData(redoData, 0, 0);
            }  
        });

        document.getElementsByClassName("option-btn")[11].addEventListener('click', ()=>{
            this.saveHistory();
            this.ctx.clearRect(0, 0, canvas.width, canvas.height);
        });

        document.getElementsByClassName("option-btn")[12].addEventListener('click', ()=>{
            this.canvas.style.cursor = "url(./img/rainbow.png) 0 32, auto";
            this.mode = 12;
        });

        document.getElementById("penSlider").addEventListener('input', ()=>{
            this.lineWidth = document.getElementById("penSliderValue").innerHTML = this.ctx.lineWidth = document.getElementById("penSlider").value;            
        });

        document.getElementById("pencolorPicker").addEventListener('input', ()=>{
            this.color = this.ctx.fillStyle = this.ctx.strokeStyle = document.getElementById("pencolorPicker").value;
        });

        document.getElementById("fontSlider").addEventListener('input', ()=>{
            document.getElementById("fontSliderValue").innerHTML = this.fontSize = document.getElementById("fontSlider").value;            
        });

        document.getElementById("fontcolorPicker").addEventListener('input', ()=>{
            this.ctx.fillStyle = this.ctx.strokeStyle = document.getElementById("fontcolorPicker").value;        
        });
        
        this.canvas.addEventListener("pointerdown", e=>{
            e.preventDefault();          
            switch (this.mode) {
                case 0:
                    this.painting = true;
                    this.ctx.fillStyle = this.ctx.strokeStyle = document.getElementById("pencolorPicker").value;
                    this.saveHistory();
                    this.ctx.beginPath();
                    break;
                case 1:
                    this.ctx.globalCompositeOperation = 'destination-out';
                    this.painting = true;
                    this.saveHistory();
                    this.ctx.beginPath(); 
                    break;
                case 2:
                    this.painting = true;
                    this.ctx.fillStyle = this.ctx.strokeStyle = document.getElementById("pencolorPicker").value;
                    this.StartImg = this.ctx.getImageData(0, 0, this.width, this.height);
                    this.saveHistory();
                    this.StartX = e.offsetX;
                    this.StartY = e.offsetY;
                    break;
                case 3:
                    this.painting = true;
                    this.ctx.fillStyle = this.ctx.strokeStyle = document.getElementById("pencolorPicker").value;
                    this.StartImg = this.ctx.getImageData(0, 0, this.width, this.height);
                    this.saveHistory();
                    this.StartX = e.offsetX;
                    this.StartY = e.offsetY;
                    break;
                case 4:
                    this.painting = true;
                    this.ctx.fillStyle = this.ctx.strokeStyle = document.getElementById("pencolorPicker").value;
                    this.StartImg = this.ctx.getImageData(0, 0, this.width, this.height);
                    this.saveHistory();
                    this.StartX = e.offsetX;
                    this.StartY = e.offsetY;
                    break;
                case 5:
                    this.saveHistory();
                    break;
                case 6:
                    this.painting = true;
                    this.ctx.fillStyle = this.ctx.strokeStyle = document.getElementById("pencolorPicker").value;
                    this.StartImg = this.ctx.getImageData(0, 0, this.width, this.height);
                    this.saveHistory();
                    this.ctx.beginPath();
                    this.StartX = e.offsetX;
                    this.StartY = e.offsetY;
                    this.ctx.moveTo(this.StartX, this.StartY);
                    break;
                case 12:
                    this.painting = true;
                    this.ctx.strokeStyle = `hsl(${this.rainbowColor}, 100%, 50%)`;
                    this.saveHistory();
                    this.ctx.beginPath();
                    break;

                
                default:
                    alert("??");
            }
        });

        this.canvas.addEventListener("pointerup", e=>{
            e.preventDefault();
            switch (this.mode) {
                case 0:
                    this.ctx.lineTo(e.offsetX, e.offsetY);
                    this.ctx.stroke();
                    this.painting = false;             
                    break;
                case 1:
                    this.ctx.lineTo(e.offsetX, e.offsetY);
                    this.ctx.stroke();
                    this.painting = false;  
                    this.ctx.globalCompositeOperation = 'source-over'                  
                    break;
                case 2:
                    if(!this.circleEmpty){
                        this.drawCircle(this.StartX, this.StartY, Math.pow(Math.pow(e.offsetX - this.StartX,2) + Math.pow(e.offsetY - this.StartY,2),0.5))
                    }
                    else{
                        this.drawEmptyCircle(this.StartX, this.StartY, Math.pow(Math.pow(e.offsetX - this.StartX,2) + Math.pow(e.offsetY - this.StartY,2),0.5))
                    }
                    this.painting = false;
                    break;
                case 3:
                    if(this.rectEmpty){
                        this.ctx.strokeRect(this.StartX, this.StartY, e.offsetX - this.StartX, e.offsetY - this.StartY)
                    }
                    else{
                        this.ctx.fillRect(this.StartX, this.StartY, e.offsetX - this.StartX, e.offsetY - this.StartY)
                    }
                    this.painting = false;
                    break;
                case 4:
                    if(this.triEmpty){
                        this.drawEmptyTriangle(e.offsetX, e.offsetY)
                    }
                    else{
                        this.drawTriangle(e.offsetX, e.offsetY)
                    }
                    this.painting = false;
                    break;
                case 5:
                    this.text(e);
                    break;
                case 6:
                    this.ctx.lineTo(e.offsetX, e.offsetY);
                    this.ctx.putImageData(this.StartImg, 0, 0); 
                    this.ctx.stroke();
                    this.painting = false;
                    break;
                case 12:
                        this.ctx.lineTo(e.offsetX, e.offsetY);
                        this.ctx.stroke();
                        this.painting = false;             
                        break;
                
                default:
                    alert("??");
            }
        });

        this.canvas.addEventListener("pointermove", e=>{
            e.preventDefault();
            switch (this.mode) {
                case 0:
                    if(!this.painting){
                        return;
                    }                    
                    this.ctx.lineTo(e.offsetX, e.offsetY);
                    this.ctx.stroke();
                    break;
                case 1:
                    if(!this.painting){
                        return;
                    }                    
                    this.ctx.lineTo(e.offsetX, e.offsetY);
                    this.ctx.stroke();
                    break;
                case 2:
                    if(!this.painting){
                        return;
                    }  
                    this.ctx.putImageData(this.StartImg, 0, 0); 
                    if(!this.circleEmpty){
                        this.drawCircle(this.StartX, this.StartY, Math.pow(Math.pow(e.offsetX - this.StartX,2) + Math.pow(e.offsetY - this.StartY,2),0.5))
                    }
                    else{
                        this.drawEmptyCircle(this.StartX, this.StartY, Math.pow(Math.pow(e.offsetX - this.StartX,2) + Math.pow(e.offsetY - this.StartY,2),0.5))
                    }
                    break;
                case 3:
                    if(!this.painting){
                        return;
                    }   
                    this.ctx.putImageData(this.StartImg, 0, 0); 
                    if(this.rectEmpty){
                        this.ctx.strokeRect(this.StartX, this.StartY, e.offsetX - this.StartX, e.offsetY - this.StartY)
                    }
                    else{
                        this.ctx.fillRect(this.StartX, this.StartY, e.offsetX - this.StartX, e.offsetY - this.StartY)
                    }
                    break;
                case 4:
                    if(!this.painting){
                        return;
                    }   
                    this.ctx.putImageData(this.StartImg, 0, 0); 
                    if(this.triEmpty){
                        this.drawEmptyTriangle(e.offsetX, e.offsetY)
                    }
                    else{
                        this.drawTriangle(e.offsetX, e.offsetY)
                    }
                    break;
                case 5:

                    break;
                case 6:
                    if(!this.painting){
                        return;
                    } 
                    this.ctx.putImageData(this.StartImg, 0, 0);         
                    this.ctx.beginPath();
                    this.ctx.moveTo(this.StartX, this.StartY);
                    this.ctx.lineTo(e.offsetX, e.offsetY);
                    this.ctx.stroke();
                    break;
                case 12:
                    if(!this.painting){
                        return;
                    } 
                    
                    this.ctx.strokeStyle = `hsl(${++this.rainbowColor}, 100%, 50%)`;                   
                    this.ctx.lineTo(e.offsetX, e.offsetY);
                    this.ctx.stroke();
                    this.ctx.beginPath();
                    this.ctx.moveTo(e.offsetX, e.offsetY);
                    break;
                
                default:
                    alert("??");
            }
        });
    }

    saveHistory(){
        const imgData = this.ctx.getImageData(0, 0, this.width, this.height);
        this.undoStack.push(imgData);
    }

    drawCircle(x, y, radius){
        // console.log("drawCircle");
        this.ctx.beginPath();
        this.ctx.arc(x, y, radius, 0, Math.PI*2, true);
        this.ctx.fill();
    }

    drawEmptyCircle(x, y, radius){
        // console.log("drawEmptyCircle");
        this.ctx.beginPath();
        this.ctx.arc(x, y, radius, 0, Math.PI*2, true);
        this.ctx.stroke();
    }

    drawTriangle(x, y){
        // console.log("drawTriangle");
        this.ctx.beginPath();
        this.ctx.moveTo(this.StartX, this.StartY);
        this.ctx.lineTo(x, y);
        this.ctx.lineTo(this.StartX - (x - this.StartX), y);
        this.ctx.lineTo(this.StartX, this.StartY)
        this.ctx.fill();
    }

    drawEmptyTriangle(x, y){
        // console.log("drawEmptyTriangle");
        this.ctx.beginPath();
        this.ctx.moveTo(this.StartX, this.StartY);
        this.ctx.lineTo(x, y);
        this.ctx.lineTo(this.StartX - (x - this.StartX), y);
        this.ctx.lineTo(this.StartX, this.StartY)
        this.ctx.stroke();
    }

    downloadImage(){
        console.log("downloadImage");
        let link = document.createElement("a");
        link.href = this.canvas.toDataURL();
        link.download = 'painting.png'
        link.click();
        link.remove();
    }

    reInit(){
        this.width = this.canvas.width = window.innerWidth * 0.8;
        this.height = this.canvas.height = window.innerHeight * 0.8;
        this.ctx = this.canvas.getContext("2d");
        this.ctx.lineWidth = this.lineWidth;
        this.ctx.lineCap = "round";
        this.ctx.lineJoin = "round";
        this.ctx.fillStyle = this.color;
        this.ctx.strokeStyle = this.color;
    }



    text(e){
        var text=document.createElement("input");
        var text_area=document.getElementById("canvasBackground");
        text.style.position="absolute";
        console.log(e.clientX);
        console.log(e.clientY);
        text.style.left=`${e.clientX}px`;
        text.style.top=`${e.clientY}px`;
        text_area.appendChild(text);
        this.ctx.font = `${this.fontSize}px ${document.getElementById('selectFont').options[document.getElementById('selectFont').selectedIndex].value}`;
        this.ctx.fillStyle = this.ctx.strokeStyle = document.getElementById("fontcolorPicker").value;   
        this.ctx.moveTo(e.offsetX, e.offsetY);
        this.ctx.beginPath();
        let x = e.offsetX;
        let y = e.offsetY;
        let ctx = this.ctx;
        text.addEventListener("keydown", function(event){
            console.log(text.value);
            if(event.keyCode==13){
                ctx.fillText(`${text.value}`, x, y);
                ctx.stroke();
                text.remove();
            }
        });
        this.ctx.closePath();
    }

  
    

    
    
   

    


    
}



const canvas = document.getElementById('canvas');
const painting = new Painter(canvas);

function resizedw(){
    var img = painting.ctx.getImageData(0, 0, canvas.width, canvas.height);
    // window.location.reload();
    painting.reInit(canvas);
    painting.ctx.putImageData(img, 0, 0, 0, 0, painting.width, painting.height);
    painting.saveHistory();
}

var doit;
window.onresize = function(){
  clearTimeout(doit);
  doit = setTimeout(resizedw, 300);
};

